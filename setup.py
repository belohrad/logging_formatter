#!/usr/bin/env python3

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

# required to be installed under debian: mysql-client, libmysqlclient-dev

setup(name='logging_formatter',
      version='0.1',
      packages=['logging_formatter', ],
      description="Colorful logger output",
      author='David Belohrad',
      author_email='david.belohrad@cern.ch',
      )
